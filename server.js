// nodemon - hot reload or live server

// Declaring Express.js
const express = require('express');
const cors = require('cors');
const api = express();
const mongoose = require('mongoose');  //Mongoose ODM
const viewerRoute = require('./routes/viewer');

api.use(cors());

/*Connect to MongoDB Atlas via Mongoose and various settings*/
mongoose.connect('mongodb+srv://admin:Mabilis42@wdc028-course-booking.6sus9.mongodb.net/dbBooking_App?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

// Sample structure for connection to DB
// mongoose {
// 	connection {
// 		status:  "error",
// 		once: () => {}
// 		on: () => {}
// 	}
// }
// To check if our DB is connected
let db = mongoose.connection;
// Notification to be displayed in console if there's an error encountered
db.on('error', (console.error.bind(console, 'Connection Error: ')));
// Once connected, show notification
db.once('open', () => {console.log('Database Connection Successful')});

/*Middleware - built-in function of the app*/

// express.json() - it has features of codes that we can use. It tells our app that all request that we will get is a JSON format
api.use(express.json()); //to recognize incoming requests as a JSON object
api.use(express.urlencoded({extended: true})); //to recognize incoming requests as strings or arrays
// req.body - It has a parsing to analyze a string/text and converting it to an object to make it readable to our database.

// Defines Schema Constructor, when we want to create a Schema this is done via Schema() constructor of the mongoose module
// new keyword - stores new object into our variable
const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	status: {
		type: String,
		default: 'pending'
	}
});

const Task = mongoose.model('Task', taskSchema); //compiles what the Schema constructor contains
// MVC = model (blueprints), views (user interface), controllers (logic)

/*Create a user schema using Mongoose with the following fields:
	username: String, email: String, password: String
	
	tasks is an array of embedded task documents, it should refer to taskSchema
*/
const userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	task: [taskSchema] //this is an array of embedded documents
});

const User = mongoose.model('User', userSchema);

// REGISTERING A NEW USER
api.post('/user', (req, res) => {
	
	// Check if username or email are duplicate prior to registration
	User.find({
		$or: [{
				username: req.body.username
			},
			{
				email: req.body.email
			}
		]
	},
		(findErr, duplicates) => {
			if (findErr) {
				return console.error(findErr);
			}
			
			// If duplicate, notify the user
			if (duplicates.length > 0) {
				return res.status(403).json({
					message: "Duplicates found, kindly choose different username and/or email."
				});
			} else {
				
				// Instantiate a new user object with properties derived from the request body
				let newUser = new User ({
					username: req.body.username,
					email: req.body.email,
					password: req.body.password,
					task: []   // Task is initially an empty array
				});
				
				// Save in DB
				newUser.save((saveErr, newUser) => {
					
					// If an error occured while saving, notify the user
					if (saveErr) {
						return console.error(saveErr);
					}
					
					// Successful save
					return res.status(201).json({
						message: `User ${newUser.username} successfully registered.`,
						data: {
							username: newUser.username,
							email: newUser.email,
							link_to_self: `/user/${newUser._id}`
						}
					});
				});
			}
		}	
	);
})

// Displaying the user details
api.get('/user/:id', (req, res) => {
	User.findById(req.params.id, (err, user) => {
		if (err) {
			return console.error(err);
		}
		return res.status(200).json({
			message: "User retrieved successfully",
			data: {
				username: user.username,
				email: user.email,
				task: `/user/${user._id}/task`
			}
		});
	});
})

// Create a new task for a specific user
/*
	1. let's create a route
	2. look for the user using the query findById
	3. if error, return error
	4. if the user doesn't have any tasks yet, add a new task and save
	5. else look for duplicates,if there is a duplicate, return the task is already registered
	6. if no duplicates found, save the task
*/

api.post('/user/:id/task', (req, res) => {
	User.findById(req.params.id, (findErr, user) => {
		if (findErr) {
			return console.error(findErr);
		}
		if (user.task === []) {
			user.task.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser) => {
				if (saveErr) {
					return console.error(saveErr);
				}
				return res.status(201).json({
					message: `${user.task[0].name} has been added.`,
					data: modifiedUser.task[0]
				});
			});
		} else {
			let dupes = user.task.filter(task => task.name.toLowerCase() === req.body.name.toLowerCase());
			if (dupes.length > 0) {
				return res.status(403).json({
					message: `${req.body.name} is already registered as a task.`
				});
			} else {
				user.task.push({
					name: req.body.name
				});
				user.save((saveErr, modifiedUser) => {
					if (saveErr) {
						return console.error(saveErr);
					}
					return res.status(201).json({
						message: `Added new task successfully`,
						data: modifiedUser.task[modifiedUser.task.length-1]
					});
				});
			}
		}
	});
});


// get details of a specific task of a user
/*
	1. create routes
	2. find the specific user, if error, return the error
	3. access the specific task of a user if task is null, return can't be found
	4. else return task with id <taskId> found
*/

// Routes
api.use('/', viewerRoute);


// Creation of port or communication endpoint
const port = 420;

// To check if port is working
api.listen(port, () => {
	console.log(`Running on ${port}`)
})