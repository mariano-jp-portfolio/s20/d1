// Importing Express.js
const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

// GET
// router.get('/get1', (request, response) => {
// 	response.send('List of Viewers');
// })

// router.get('/get2', (req, res) => {
// 	res.json({
// 		"name": "Paolo",
// 		"gender": "male"
// 	});
// })


// request.query
// router.get('/fetch', (req, res) => {
// 	// console.log(req.query);
// 	res.send(req.query);
// })


// POST
// router.post('/insert', (req, res) => {
// 	res.json({
// 		"message": "Success!"
// 	});
// })

// router.post('/post', (req, res) => {
// 	res.json({
// 		"name": "Bailey",
// 		"lastName": "Black",
// 		"message": "You are awesome!"
// 	});
// })

// router.post('/insert', (req, res) => {
// 	console.log(req.body)
// })


// // Destructuring
// router.get('/fetch', (req, res) => {
// 	const {name, age, gender} = req.query;
	
// 	if (age == 28) {
// 		res.status(200).send({
// 			name,
// 			age,
// 			gender
// 		});
// 	} else {
// 		res.status(500).send({
// 			"message": "Age is not equal.",
// 			"status": "Failed"
// 		});
// 	}
// });


// Request data from our HTML document
// api.get('/', (req, res) => {
// 	{
// 		username: req.body.username
// 	},
// 	{
// 		email: req.body.email
// 	}
// })

// // REGISTERING A NEW USER
// router.post('/user', (req, res) => {
	
// 	// Check if username or email are duplicate prior to registration
// 	User.find({
// 		$or: [{
// 				username: req.body.username
// 			},
// 			{
// 				email: req.body.email
// 			}
// 		]
// 	},
// 		(findErr, duplicates) => {
// 			if (findErr) {
// 				return console.error(findErr);
// 			}
			
// 			// If duplicate, notify the user
// 			if (duplicates.length > 0) {
// 				return res.status(403).json({
// 					message: "Duplicates found, kindly choose different username and/or email."
// 				});
// 			} else {
				
// 				// Instantiate a new user object with properties derived from the request body
// 				let newUser = new User ({
// 					username: req.body.username,
// 					email: req.body.email,
// 					password: req.body.password,
// 					task: []   // Task is initially an empty array
// 				});
				
// 				// Save in DB
// 				newUser.save((saveErr, newUser) => {
					
// 					// If an error occured while saving, notify the user
// 					if (saveErr) {
// 						return console.error(saveErr);
// 					}
					
// 					// Successful save
// 					return res.status(201).json({
// 						message: `User ${newUser.username} successfully registered.`,
// 						data: {
// 							username: newUser.username,
// 							email: newUser.email,
// 							link_to_self: `/user/${newUser._id}`
// 						}
// 					});
// 				});
// 			}
// 		}	
// 	);
// })


module.exports = router;